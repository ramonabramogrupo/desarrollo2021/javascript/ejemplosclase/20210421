/*
Creamos un array con los dias de la semana
*/

let dias=[
	"Lunes",
	"Martes",
	"Miercoles",
	"Jueves",
	"Viernes"
];



// Leemos el dia 2

console.log(dias[2]); // seria el dia 3 que esta en la posicion 2
console.log(dias[1]); // seria el dia 2 que esta en la posicion 1

// Leemos el dia 5

console.log(dias[4]);

// Colocamos la tilde a Miércoles

dias[2]="Miércoles";

// Añadimos el sabado y el domingo

// un poco lio porque tienes que saber donde termina el array para
// añadir los nuevos elementos
//dias[5]="Sabado";
//dias[6]="Domingo";

// mas comodo
dias.push("Sabado");
dias.push("Domingo");